A critical part of Wine testing is to use or test real applications
regularly and report regressions as soon as possible to the Wine
developers. This way regressions are caught early. That means the set of
changes to search through is still small, the developers who made the
changes are more likely still active, and their changes will be fresh in
their minds.

Another reason why your contribution is especially valuable is that
Wine developers probably do not have access to the application (they
cannot buy all the applications out there) or do not know it well
enough to test it properly. You can help even further by becoming a
[maintainer](Maintainer-Guidelines), that administers an application's
entry in [AppDB](https://appdb.winehq.org/). Many applications, which
aren't supported out of the box yet, can run (or run better) by using
just the right mix of native (Windows) and built-in (Wine)
libraries. By testing the application with various mixes of libraries
and sharing your results in the AppDB, you can help other Wine users
successfully run the application, making Wine useful to a greater
number of people.

Before choosing to become a maintainer, you should have a good
understanding of your application, although coding skills are *not*
necessary. You should also be willing to or already use this application
regularly, testing it for at least each new Wine release. It is more
important to test one application regularly than ten applications once a
year.

# Hints & Tips

If you already are an AppDB Maintainer for the Wine project, or are
interested in becoming one, here are some hints and tips that will
simplify things for you.

## Keeping track of new bugs

Many [bug reports](https://gitlab.winehq.org/wine/wine/-/wikis/Bugs) that may affect your apps might not
be linked to the proper AppDB entries on their own. Don't worry though
because there is an easy way to keep track of new bugs. Bugzilla offers
news feeds for bug reports. To track bugs for your apps, first go visit
the [bug tracker](https://bugs.winehq.org/) and search with the name of
your maintained app. At the end of the search results you will find a
link called "Feed," which will lead you to a news feed. Subscribe this
URL in your favorite feed reader and never miss a new bug again.

## Reporting Regressions

If you find any regressions, please run a [regression
test](https://gitlab.winehq.org/wine/wine/-/wikis/Regression-Testing) and [file a
bug](https://gitlab.winehq.org/wine/wine/-/wikis/Bugs).

## Creating a How-To

As a maintainer, you can create an official "How-To" for an application,
with instructions on how to get the program running under Wine. By
collecting and testing different comments by visitors and across
versions, you can stay on top of obsolete and contradictory advice. This
not only makes it easier for users to get the application working, but
it can simplify the debugging process for testers and developers.
