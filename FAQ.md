This FAQ answers questions related to the usage of the [WineHQ
Application Database](https://appdb.winehq.org/).

## What is the Application Database?

The Application Database is a repository for Windows application
compatibility information with Wine. In particular it provides the
following information: (See the [Visual
FoxPro](https://appdb.winehq.org/objectManager.php?sClass=application&iId=296)
entry for an example)

- Whether a given application works at all with Wine.
- If it is partially working, then which areas of the application have
  problems.
- How to install and get that application working best.

And if the Windows application you want most to use with Wine does
not work, it lets you vote for it to bring it to the attention of Wine
developers.

## What are the benefits of this Application Database?

The Application Database benefits both Wine developers and users. It
lets Wine developers know which applications the community most wants to
see working. And it lets Wine users know beforehand if their application
will work in Wine, and also what are the tricks to get it working best.

## How does a new application get added to the database?

Any registered user can submit an application. Use the [Submit
App](https://appdb.winehq.org/objectManager.php?sClass=application_queue&sTitle=Submit+Application&sAction=add)
link in the AppDB sidebar to add an entry; it will be processed by an
admin before becoming visible to all users. You can make changes to your
queued entries by visiting the [Your Queued
Items](https://appdb.winehq.org/queueditems.php) page.

ℹ For more info on submitting new applications, new versions, and even
test results to AppDB, see the [Test Results
Guidelines](Test-Results-Guidelines)

## How long does it take for test reports to be processed?

Maintainers have 7 days to process test reports. They are automatically
removed if they fail to do so, and the queued test reports are then
reviewed by an admin. Submissions for entries that do not have a
maintainer are processed by the admins. If your test remains in the
queue for more than 7 days, report it on the [Website
Issues](https://forum.winehq.org/viewforum.php?f=11) section of the
forum.

## How can I submit screenshots?

We love to hear from our users. Send those helpful screenshots by
clicking on any screenshot image you will see in the specific version
page of the application you want to add a screenshot for. If you are not
a maintainer, your screenshot will be reviewed and will then be added to
the database.

## How can I submit how-to's?

If you are not a maintainer, you can't create new official How-to's. So
the best way to proceed is to post your How-to as a comment in the
specific version page of the application of interest. A maintainer might
then choose to add your How-to as an official How-to for this database
entry.

## How do I become an Application Database maintainer?

If you are submitting a new application or version entry, the submission
form will have a section asking if you want to be a maintainer; simply
click yes. To apply to be a maintainer or supermaintainer of an existing
entry, click on the button you will find in the application overview or
specific version page to send your request. Clearly describe your
qualifications and reasons for applying. An administrator will then
review your proposal and add you to the maintainer's team for this
application or version if your application is approved.

## What is the difference between a super maintainer and a maintainer?

A super maintainer maintains an application and all its versions. A
maintainer maintains only one specific version of an application.

## What is meant by the term **vendor**?

The vendor is the developer of an application.

## Contacting Us

If you have more questions or comments please contact us on [our
forums](https://forum.winehq.org/).
