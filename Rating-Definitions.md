The Application Database features a rating system that allows you to see
which applications work best in Wine. This rating system is designed to
assist users by giving a rating based on other users' experience.

### Platinum

- Works as well as (or better than) on Windows out of the box.

### Gold

- Works as well as (or better than) on Windows with workarounds.

### Silver

- Works excellently for normal use, but has some problems for which
  there are no workarounds.

### Bronze

- Works, but has some problems for normal use.

### Garbage

- Problems are severe enough that it cannot be used for the purpose it
  was designed for.
