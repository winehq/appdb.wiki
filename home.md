---
title: Wine Application Database
---

You'll find here the available documentation about the [Wine
Application Database](https://appdb.winehq.org/).

## Contents

- [Frequently Asked Questions](FAQ) about the Wine Application
  Database.

- [Maintainers](Maintainers): How to become an Application Database
  maintainer?

- [Test Results Guidelines](Test-Results-Guidelines): How to submit
  test results to the Application Database?

- [Voting Help](Voting-Help): How to vote for your favorite
  application?

- [Rating Definitions](Rating-Definitions): What do the Application
  Database ratings mean?

- [Maintainer Guidelines](Maintainer-Guidelines): Guidelines for
  application maintainers.

- [ToDo List](ToDo-List) for developers.
