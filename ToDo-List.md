The following is a list of in progress to do items for the WineHQ
Application Database.

To file bugs for the AppDB, please use
[Bugzilla](https://bugs.winehq.org).

**Note:** If you want to help update the AppDB, please [submit merge
requests](https://gitlab.winehq.org/wine/wine/-/wikis/Submitting-Patches) to the [AppDB project on
Gitlab](https://gitlab.winehq.org/winehq/appdb).

  - Fix PHP warning messages
  - ~~Move static help documentation pages to the Wiki (bug
    #41473)~~ - completed
  - Update site layout to the WineHQ responsive design (bug #40394)
      - ~~change design to use bootstrap~~ - completed
      - ~~convert static icons to FontAwesome~~ - completed
      - move static assets to the CDN mirror (Fastly)
  - Update to modern PHP coding standards (work with PHP 7.0+)
    (bug #35355)
  - Admins need spam controls (bug #31973)
  - ~~Emails have trimmed URLs (bug #33725)~~ - completed
  - ~~Broken email encoding (bug #17276)~~ - completed
  - ~~Don't delete users after 6 months of inactivity (bug
    #19201)~~ - completed
  - ~~Distinguish between staging and development versions (bug
    #39382)~~ - completed
  - Improve security:
      - sanitize user input from wysiwyg fields (bug #34647)
      - validate testedRelease field input (bug #22798)
      - add flood control for test submissions (bug #21639)

[Open AppDB bugs in
Bugzilla](https://bugs.winehq.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=STAGED&bug_status=REOPENED&list_id=417640&product=WineHQ%20Apps%20Database&query_format=advanced)
