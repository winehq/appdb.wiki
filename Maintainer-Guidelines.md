This page gives information on how to maintain an application once you
are an application maintainer. If you are looking for information on how
to become an application maintainer, this topic is covered in the
[FAQ](FAQ).

## General Maintainer Information

Becoming a (super) maintainer gives you certain rights on the
Application Database. However as you may know, with great power, comes
great responsibility...

An application maintainer is someone who runs the application regularly
and who is willing to be active in reporting regressions with newer
versions of Wine and to help other users run this application under
Wine.

### You are expected to:

- Keep the application comments clean, all stale comments should be
  removed.
- Be an active user of that application and version.
- Keep up-to-date with all Wine releases, if there are regressions they
  should filed as bugs in [Bugzilla](https://bugs.winehq.org/) if there
  are not bugs already filed.
- Periodically submit your own test reports for the versions you
  maintain. Inactive maintainers (currently defined as not logging in
  for two years) are automatically removed.
- Process submitted test reports in a timely manner. Maintainers who
  fail to process test reports within 7 days are automatically removed
  by the system.

### You will:

- Receive an email when a comment is posted or deleted for the
  application or the application information is modified
- Be able to modify all aspects of your application(s) and version(s),
  including the description, screenshots, HOWTOs, notes, comments, etc.

## Application overview page guidelines

*N.B. Only super maintainers can change the general description of an
application.* You can edit an application overview page by clicking on
the "Edit Application" button in the application overview page. You will
be then presented with a form whose fields are explained thereafter.

- Application name should be checked against official application name
  and should not contain the vendor's name.
- Application vendor should be set correctly. If the vendor is missing,
  please report on forums.
- Keywords are used by the search engine. You don't need to repeat the
  application's name but you are encouraged to provide some keywords in
  this field.
- Description should describe the application in a general manner (what
  it does). The Wine and version specific considerations go in
  version-specific pages (described below). If you want to add more than
  a short sentence, please add a carriage return after the first
  sentence as it will be used when showing the application summary in
  the search engine, top-25 page and so on.
- Webpage should contain the home page for this application. Please
  remember that a URL starts with `http://`.

## Version page guidelines

You can edit a version page by clicking on the "Edit Version" button in
any version page for which you are a (super) maintainer. You will then
be presented with a form whose fields are explained thereafter.

- Version name should be checked against official version name. However
  if you feel that two minor revisions of an application will share the
  same problems or success, you are encouraged to use wildcards in the
  version number to avoid cluttering the database with hundreds of
  versions. For example if you know that Adobe Acrobat 6.0, 6.01 and 6.1
  are about the same application, just choose 6.x as a version name. You
  can however use the full version name in a history table in the
  description field (see below).
- Description should describe the application version and not be a
  repetition of the application description. Please use a short sentence
  as the first line of your description to describe what this version
  is, and then add a carriage return after this first sentence as it
  will be used when showing version summary, in the application page for
  example.
- Rating lets you rate how this version ran with the latest Wine version
  you tested. For rating definitions, please see: [Rating
  Definitions](Rating-Definitions).
- Release is the latest release of Wine you tested this version with.

## Additional URLs guidelines

In both application and version editing pages you can add additional
URLs. These URLs can, for example, help the user find native
alternatives for this application (in application overview page) or find
specific dlls (in version page).

## Screenshots guidelines

As a maintainer you are encouraged to provide screenshots for the
versions you tested under Wine. To add a screenshot, simply click on the
screenshot thumbnail in the version page.

## Notes guidelines

In the version page you can add, edit and delete notes for the versions
you maintain. Just click on the "Add note" button.

## HOWTO guidelines

HOWTOs are just a special kind of notes. You can provide step-by-step
explanations on how to install or run a specific version of the current
application here. Please try to keep HOWTOs up to date (for example some
steps might no longer be needed if using a more recent version of Wine).

## Warnings guidelines

Warnings are just a special kind of note. Warnings appear in red to
catch the attention of the reader. They can be used to warn users
against potential data corruption for instance. Please keep the warning
up-to-date, especially if it is not true when using a recent Wine
version (in this case you might choose to keep the warning and tell
users that since Wine X.Y, this has been fixed).

## Comments guidelines

As a maintainer you are entitled to delete comments. Use this feature
wisely and always explain why you deleted a comment.

Comments containing any of the following should be deleted immediately:

- Links to illegal downloads or discussions of how to obtain or use them
- Spam
- Off-topic discussions

Users who repeatedly post such comments should be reported on [the
Website Issues section of the
forum](https://forum.winehq.org/viewforum.php?f=11).

If a comment is outdated, you can safely delete it. Whenever you can,
extract how-tos, other people's tests (for your test history table for
example) from the comments and put it in your versions page and how-to
notes.

If users paste large amounts of debug/crash output from wine, it might
be a good idea to delete their comment and depending on whether or not a
bug report as been filed for the problem, ask them to file a report and
attach the information.

## Using the HTML editor

The HTML editor allows users of the AppDB to write richer descriptions,
guides or warnings by providing a WYSIWYG HTML editor. The HTML editor
can be disabled in your Preferences which will allow you to edit the
html source of various fields.

## Contacting Us

If you have additional questions, please do not hesitate to contact us
on our [forums](https://forum.winehq.org/).
