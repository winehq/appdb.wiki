The Application Database features a voting system that allows you to
pick which **3** application versions you would MOST like to see
running in Wine.

## Step by Step Help on Voting

- Log in to the Application Database.
- Browse to the application version you wish to add to your vote list.
- In the table showing name, license, rating etc., click on **Vote**.
- Select the slots you wish to use, and press **Submit**.
- Done!

## Voting System Notes

- *Please* vote for applications which will benefit the community.
  Don't vote for applications that are known to be working well. We know
  Solitaire works. Voting for it would not make much sense.
- You can clear your vote at any time. Browse to any application
  version, select **Vote** from the table showing name, license, rating
  etc., then choose the slots you wish to clear and press **Delete**.
